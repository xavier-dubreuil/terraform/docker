terraform {
  required_providers {
    docker = {
      source  = "kreuzwerker/docker"
      version = "2.19.0"
    }
  }
}

variable "registries" {
  type    = list(map(any))
  default = []
}

provider "docker" {

  dynamic "registry_auth" {
    for_each = var.registries
    iterator = registry
    content {
      address  = lookup(registry.value, "address")
      username = lookup(registry.value, "username", "")
      password = lookup(registry.value, "password", "")
    }
  }
}

data "docker_network" "generic" {
  name = "generic"
}

data "docker_network" "traefik" {
  name = "traefik"
}

output "networks" {
  value = {
    generic = data.docker_network.generic.name
    traefik = data.docker_network.traefik.name
  }
}
